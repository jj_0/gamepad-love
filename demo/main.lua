Object = require "../libs/classic"

Scene = Object.extend(Object)

-- Scene
function Scene.new(self)
    --
end

function Scene.draw(self)
    --
end

function Scene.update(self)
    --
end

--

sc_HelloWorld = Object.extend(Scene)


function sc_HelloWorld.uprise(x)
    return 5 - math.abs(((x / 2) % 20) - 10)
end

function sc_HelloWorld.new(self)
    self.x = 600
end

function sc_HelloWorld.draw(self)
    str = "Hello World"
    for char=0, string.len(str) , 1 do
        xl = self.x + (char * 12)
        love.graphics.print(str:sub(char, char), xl, 300 + self.uprise(xl))
    end
end

function sc_HelloWorld.update(self)
    self.x = self.x - 1
    if self.x <= -100 then
        self.x = 1000
        print("Reset")
    end
    if love.joystick.getJoysticks()[1]:getHat(1) == "u" then
        activeScene = sc_Matrix()
    end
end

---


sc_Matrix = Object.extend(Scene)

function sc_Matrix.new(self)
    self.tick = 0
end

function sc_Matrix.update(self)
    self.tick = self.tick + 1
    if love.joystick.getJoysticks()[1]:getHat(1) == "d" then
        activeScene = sc_HelloWorld()
    end
end

function sc_Matrix.draw(self)
    for x = 0, 80, 1 do
        coef1 = math.abs(25 - (self.tick % 50)) / ( x % 7 )
        coef2 = ( 1 - ( 2 * ( x % 2 ) ) ) * (12 * ( x % 3 ) ) * 20
        coef3 = ( 4 - ( x % 9 ) ) * 4
        y = (self.tick + coef1 + coef2 + coef3) % 600
        love.graphics.print({{0,100,0,1}, "M"}, x*10, y-22, 0, 0.4, 0.4)
        love.graphics.print({{0,200,0,1}, "M"}, x*10, y-16, 0, 0.6, 0.6)
        love.graphics.print({{0,200,0,1}, "M"}, x*10, y-10, 0, 0.8, 0.8)
        love.graphics.print({{0,255,0,1}, "M"}, x*10, y   , 0,   1,   1)
    end
end

---

function love.load()
    --------
    joystick_table = love.joystick.getJoysticks()
    for key, value in pairs(joystick_table) do
        print(key, value, value:getName())
    end

    -- activeScene = sc_HelloWorld()
    activeScene = sc_Matrix()
end

function love.draw()
    activeScene.draw(activeScene)
end

function love.update()
    activeScene.update(activeScene)
    --print(love.joystick.getJoystickCount())
    -- print(love.joystick.getJoysticks()[1]:isDown(1))
    -- print(love.joystick.getJoysticks()[1]:getHat(1))
end