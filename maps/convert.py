from PIL import Image

##   0) [255,255,255] Free space - may change to points
##   1) [  0,  0,  0] Wall
##   2) [  0,255,255] Power points - power up for pacmans
##   8) [255,  0,  0] Ghost spawnpoints
##   9) [255,255,  0] Pacman spawnpoint

def convert(filename):
    source = Image.open(filename)
    data = source.convert('RGB')
    for y in range(data.height):
        print("{", end='')
        for x in range(data.width):
            pixel = data.getpixel((x,y))
            if pixel == (255,255,255):
                print("0, ", end='')
            if pixel == (0,0,0):
                print("1, ", end='')
            if pixel == (0,255,255):
                print("2, ", end='')
            if pixel == (255,0,0):
                print("8, ", end='')
            if pixel == (255,255,0):
                print("9, ", end='')
        print("},")



convert("maps/002.bmp")