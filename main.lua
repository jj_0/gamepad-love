Object = require "../libs/classic"

-- INPUTS

playerInput = Object.extend(Object)

function playerInput.new(self)
    self.up = false
    self.down = false
    self.left = false
    self.right = false
    self.a = false
    self.b = false
    self.direction = 'none'
    self.lastDirection = 'none'
    self.role = ''
    self.id = 'none'
    self.ready = false
    self.name = nil
end

function playerInput.getName(self)
    if self.name then
        return self.name
    end
    return self.id
end

function playerInput.checkReady(self)
    if (not self.ready) and self.a then
        self.ready = true
        if self.js then self.js:setVibration(0.5, 0, 0.5) end
    elseif self.ready and self.b then
        if self.js then self.js:setVibration(0, 0.5, 0.5) end
        self.ready = false
    end
end

function playerInput.update(self)
    --
end

function playerInput.getInput(self)
    return {self.direction, self.a, self.b}
end

function playerInput.printValues(self)
    print("up", self.up)
    print("down", self.down)
    print("left", self.left)
    print("right", self.right)
end

function playerInput.getLastDirection(self)
    return self.lastDirection
end

playerInputKeyboard = Object.extend(playerInput)

function playerInputKeyboard.new(self, id, up_key, down_key, left_key, right_key, a_key, b_key)
    self.keys = {
        up = up_key,
        down = down_key,
        left = left_key,
        right = right_key,
        a = a_key,
        b = b_key
    }
    self.id = id
end

function playerInputKeyboard.update(self)
    self.up = love.keyboard.isDown(self.keys.up)
    self.down = love.keyboard.isDown(self.keys.down)
    self.left = love.keyboard.isDown(self.keys.left)
    self.right = love.keyboard.isDown(self.keys.right)
    self.a = love.keyboard.isDown(self.keys.a)
    self.b = love.keyboard.isDown(self.keys.b)

    -- TODO: Find more elegant solution
    x = 0
    y = 0
    if self.up then
        y = y - 1
    end
    if self.down then
        y = y + 1
    end
    
    if self.left then
        x = x - 1
    end
    if self.right then
        x = x + 1
    end

    if x == 0 and y ~= 0 then
        if y == 1 then
            self.direction = 'down'
            self.lastDirection = 'down'
        else
            self.direction = 'up'
            self.lastDirection = 'up'
        end
    elseif x ~= 0 and y == 0 then
        if x == 1 then
            self.direction = 'right'
            self.lastDirection = 'right'
        else
            self.direction = 'left'
            self.lastDirection = 'left'
        end
    else
        self.direction = 'none'
    end
end

playerInputJoystick = Object.extend(playerInput)

function playerInputJoystick.new(self, joystick)
    self.js = joystick
    self.id, _ = joystick:getID()
end

function playerInputJoystick.getName(self)
    if self.name then
        return self.name
    end
    return self.js:getName()
end

function playerInputJoystick.update(self)
    dir = self.js:getHat(1)
    -- print(dir)
    if dir == "c" then self.direction = 'none' end
    if dir == "d" then
        self.direction = 'down'
        self.lastDirection = 'down'
    end
    if dir == "u" then
        self.direction = 'up'
        self.lastDirection = 'up'
    end
    if dir == "l" then
        self.direction = 'left'
        self.lastDirection = 'left'
    end
    if dir == "r" then
        self.direction = 'right'
        self.lastDirection = 'right'
    end

    self.up = dir == "u"
    self.down = dir == "d"
    self.left = dir == "l"
    self.right = dir == "r"

    self.a = self.js:isDown(1)
    self.b = self.js:isDown(2) -- Not sure, but will solve later

end

inputs = {}

function inputsUpdate()
    for key, value in pairs(inputs) do
        value.update(value)
        -- value.printValues(value)
    end
end


-- THE GAME:

-- -- Support

function coorToTile(value)
    return math.floor(value / 60)
end

function sign(num)
    return num>0 and 1 or num<0 and -1 or 0
end

function round(num, mult)
    a = num / mult
    if a % 1 < 0.5 then
        return math.floor(a) * mult
    end
    return math.floor(a + 1) * mult
end

-- -- Player

player = Object.extend(Object)

function player.new(self, input, gameitself, colorset)
    self.game = gameitself
    self.input = input
    self.x = 180
    self.y = 180
    self.speed = 3
    self.speedX = 0
    self.speedY = 0
    self.turnAble = false
    self.score = 0
    if colorset then self.colorset = colorset else self.colorset = {1,1,1,1} end
end

function player.setPosition(self, tileX, tileY)
    self.x = tileX * 60
    self.y = tileY * 60
end

function player.draw(self)
    love.graphics.setColor(self.colorset)
    love.graphics.rectangle("fill", self.x, self.y, 10, 10)
end

function player.update(self)
    d = self.input.getLastDirection(self.input)
    if self.y % 60 < self.speed or self.speedX ~= 0 then
        if d == 'right' and getTileValue(self.game.activeLevel, coorToTile(self.y), coorToTile(self.x) + 1) ~= 1 and (self.speedX == 0 or self.turnAble) then
            self.speedX = 1
            self.speedY = 0
            -- print("Set right")
        end
        if d == 'left' and getTileValue(self.game.activeLevel, coorToTile(self.y), coorToTile(self.x) - 1) ~= 1  and (self.speedX == 0 or self.turnAble) then
            self.speedX = -1
            self.speedY = 0
            -- print("Set left")
        end
    end
    if self.x % 60 < self.speed or self.speedY ~= 0 then
        if d == 'up' and getTileValue(self.game.activeLevel, coorToTile(self.y) - 1, coorToTile(self.x)) ~= 1 and (self.speedY == 0 or self.turnAble) then
            self.speedX = 0
            self.speedY = -1
            -- print("Set up")
        end
        if d == 'down'  and getTileValue(self.game.activeLevel, coorToTile(self.y) + 1, coorToTile(self.x)) ~= 1 and (self.speedY == 0 or self.turnAble) then
            self.speedX = 0
            self.speedY = 1
            -- print("Set down")
        end
    end
    if (self.x % 60 < self.speed and self.speedX ~= 0) or (self.y % 60 < self.speed and self.speedY ~= 0) then
        -- print(sign(self.speedX), sign(self.speedY))
        if d ~= nil then
            -- print("Position: "..self.x.."x"..self.y, "Checking: "..(sign(self.speedX) + coorToTile(self.x)).."x"..(sign(self.speedY) + coorToTile(self.y)), "Direction: "..d)
        end
        if getTileValue(self.game.activeLevel, sign(self.speedY) + coorToTile(self.y), sign(self.speedX) + coorToTile(self.x)) == 1 then
            self.speedY = 0
            self.speedX = 0
            -- print("Value reset")
        end
    end
    if self.speedX == 0 then self.x = round(self.x, 60) end
    if self.speedY == 0 then self.y = round(self.y, 60) end
    self.moveX(self, self.speedX)
    self.moveY(self, self.speedY)
end

function player.moveX(self, speedMultiply)
    self.x = self.x + (speedMultiply * self.speed)
end

function player.moveY(self, speedMultiply)
    self.y = self.y + (speedMultiply * self.speed)
end


-- -- -- GHOST

Ghost = Object.extend(player)

function Ghost.new(self, input, gameitself, colorset)
    player.new(self, input, gameitself, colorset)
    self.image = love.graphics.newImage('sprites/ghost/base.PNG')
end

function Ghost.draw(self)
    love.graphics.setColor(self.colorset)
    love.graphics.draw(self.image, self.x, self.y)
end

-- -- PACMAN

Pacman = Object.extend(player)

function Pacman.new(self, input, gameitself, colorset)
    player.new(self, input, gameitself, colorset)
    self.turnAble = true
    self.image = love.graphics.newImage('sprites/pacman/base.PNG')
    self.x = 21 * 60
    self.y = 14 * 60
    self.speed = 4
end

function Pacman.draw(self)
    love.graphics.setColor(self.colorset)
    love.graphics.draw(self.image, self.x, self.y)
end

function Pacman.update(self)
    player.update(self)
    r = coorToTile(self.y)
    c = coorToTile(self.x)

    if getTileValue(self.game.activeLevel, r, c) == 7 and self.x % 60 < self.speed and self.y % 60 < self.speed then
        self.game.activeLevel[r+1][c+1] = 0
        self.score = self.score + 10
        print("Pacmans higher score to ", self.score)
    end

    for key, value in pairs(self.game.ghosts) do
        if (math.abs(value.x - self.x) ^ 2) + (math.abs(value.y - self.y) ^ 2) < 60 ^ 2 then
            print("CATCH")
        end
    end
end

-- -- BACKGOUND

TYLE_TYPES = {  "Bottom", "LeftBottom - open bottom", "Left",
                "LeftBottom - open left", "LeftBottom - inverted", "LeftBottom",
                "LeftTop - inverted", "LeftTop - open left",
                "LeftTop - open top", "LeftTop", "main", "Right",
                "RightBottom - inverted", "RightBottom - open bottom",
                "RightBottom - open right", "RightBottom", "RightTop - inverted",
                "RightTop - open right", "RightTop - open top", "RightTop",
                "Top"
            }

backgroud = Object.extend(Object)

function backgroud.new(self, level)
    -- tiles 32x18
    self.level = level

    self.image = {}
    self.image["base"] = {}
    for key, value in pairs(TYLE_TYPES) do
        self.image["base"][value] = love.graphics.newImage('sprites/tile/base/'..value..'.PNG')
    end
    self.dotImage = love.graphics.newImage('sprites/dot/main.png')
end

function getTileValue(table, row, column)
    if row < 0 then return 0 end
    if row > 17 then return 0 end
    if column < 0 then return 0 end
    if column > 31 then return 0 end
    return table[row+1][column+1]
end

function backgroud.draw(self)
    love.graphics.setColor(1,1,1,1)
    for r = 0, 17, 1 do
        for c = 0, 31, 1 do
            tileValue = getTileValue(self.level, r, c)
            if tileValue == 1 then
                love.graphics.draw(self.image["base"]["main"], c*60, r*60)
                if getTileValue(self.level, r, c-1) ~= 1 then
                    love.graphics.draw(self.image["base"]["Left"], c*60, r*60)
                end
                if getTileValue(self.level, r, c+1) ~= 1 then
                    love.graphics.draw(self.image["base"]["Right"], c*60, r*60)
                end
                if getTileValue(self.level, r-1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["Top"], c*60, r*60)
                end
                if getTileValue(self.level, r+1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["Bottom"], c*60, r*60)
                end
                ----------------------------------------------------------------
                if getTileValue(self.level, r-1, c+1) ~= 1  and getTileValue(self.level, r, c+1) == 1 and getTileValue(self.level, r-1, c) == 1 then
                    love.graphics.draw(self.image["base"]["RightTop - inverted"], c*60, r*60)
                end
                if getTileValue(self.level, r+1, c+1) ~= 1  and getTileValue(self.level, r, c+1) == 1 and getTileValue(self.level, r+1, c) == 1 then
                    love.graphics.draw(self.image["base"]["RightBottom - inverted"], c*60, r*60)
                end
                if getTileValue(self.level, r-1, c-1) ~= 1  and getTileValue(self.level, r, c-1) == 1 and getTileValue(self.level, r-1, c) == 1 then
                    love.graphics.draw(self.image["base"]["LeftTop - inverted"], c*60, r*60)
                end
                if getTileValue(self.level, r+1, c-1) ~= 1  and getTileValue(self.level, r, c-1) == 1 and getTileValue(self.level, r+1, c) == 1 then
                    love.graphics.draw(self.image["base"]["LeftBottom - inverted"], c*60, r*60)
                end
                ----------------------------------------------------------------
                if getTileValue(self.level, r, c+1) ~= 1 and getTileValue(self.level, r-1, c) == 1 then
                    love.graphics.draw(self.image["base"]["RightTop - open top"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) ~= 1 and getTileValue(self.level, r-1, c) == 1 then
                    love.graphics.draw(self.image["base"]["LeftTop - open top"], c*60, r*60)
                end
                if getTileValue(self.level, r, c+1) ~= 1 and getTileValue(self.level, r+1, c) == 1 then
                    love.graphics.draw(self.image["base"]["RightBottom - open bottom"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) ~= 1 and getTileValue(self.level, r+1, c) == 1 then
                    love.graphics.draw(self.image["base"]["LeftBottom - open bottom"], c*60, r*60)
                end
                ----------------------------------------------------------------
                if getTileValue(self.level, r, c+1) == 1 and getTileValue(self.level, r-1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["RightTop - open right"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) == 1 and getTileValue(self.level, r-1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["LeftTop - open left"], c*60, r*60)
                end
                if getTileValue(self.level, r, c+1) == 1 and getTileValue(self.level, r+1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["RightBottom - open right"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) == 1 and getTileValue(self.level, r+1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["LeftBottom - open left"], c*60, r*60)
                end
                ----------------------------------------------------------------
                if getTileValue(self.level, r, c+1) ~= 1 and getTileValue(self.level, r-1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["RightTop"], c*60, r*60)
                end
                if getTileValue(self.level, r, c+1) ~= 1 and getTileValue(self.level, r+1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["RightBottom"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) ~= 1 and getTileValue(self.level, r-1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["LeftTop"], c*60, r*60)
                end
                if getTileValue(self.level, r, c-1) ~= 1 and getTileValue(self.level, r+1, c) ~= 1 then
                    love.graphics.draw(self.image["base"]["LeftBottom"], c*60, r*60)
                end
            end
            if tileValue == 7 then
                love.graphics.draw(self.dotImage, c*60, r*60)
            end
        end
    end
end


-- -- -- LEVELs

-- level = {
--     {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
--     {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
--     {1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1},
--     {1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},
--     {1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1},
--     {1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1},
--     {1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1},
--     {1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1},
--     {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1},
--     {1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1},
--     {1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},
--     {1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1},
--     {1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
--     {1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1},
--     {1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1},
--     {1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1},
--     {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
--     {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
-- }

-- LEGEND:
--   0) Free space - may change to points
--   1) Wall
--   2) Power points - power up for pacmans
--   7) PacDot - poit for pacmans - may automaticaly replace 0
--   8) Ghost spawnpoints
--   9) Pacman spawnpoint

level = {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, },
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, },
    {1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 2, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 2, 1, 0, 1, },
    {1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 0, 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 8, 8, 8, 8, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 8, 8, 8, 8, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, },
    {1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, },
    {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 9, 9, 9, 9, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, },
    {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, },
    {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, },
    {1, 1, 2, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, },
    {1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 2, 1, 0, 1, },
    {1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 2, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, },
    {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, },
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, },
}

-- GAME SET

Game = Object.extend(Object)

function Game.update(self)
    --
end

function Game.draw(self)
    --
end

function Game.load(self)
    --
end

function Game.getName()
    return "No game 🥱"
end

-- PACMAN GAME

PacmanGame = Object.extend(Game)

function PacmanGame.new(self, pacmans, ghosts)
    self.pacmans = {}
    for key, value in pairs(pacmans) do
        table.insert(self.pacmans, Pacman(value.input, self, value.colorset))
    end
    self.ghosts = {}
    for key, value in pairs(ghosts) do
        table.insert(self.ghosts, Ghost(value.input, self, value.colorset))
    end
end

function PacmanGame.initLevel(self, freshLevel)
    self.activeLevel = freshLevel
    self.ghostSpawnpoints = {}
    self.pacmanSpawnpoints = {}
    for r = 0, 17, 1 do
        for c = 0, 31, 1 do
            tileValue = getTileValue(self.activeLevel, r, c)
            if tileValue == 8 then table.insert(self.ghostSpawnpoints, {x=c, y=r}) end
            if tileValue == 9 then table.insert(self.pacmanSpawnpoints, {x=c, y=r}) end
            if tileValue == 0 then self.activeLevel[r+1][c+1] = 7 end
        end
    end
end

function PacmanGame.load(self)
    self.initLevel(self, level)
    self.bg = backgroud(self.activeLevel)
    pacmanSpawnpointsCount = table.getn(self.pacmanSpawnpoints)
    for key, value in pairs(self.pacmans) do
        position = self.pacmanSpawnpoints[1 + key%pacmanSpawnpointsCount]
        value.setPosition(value, position.x, position.y)
    end
    ghostSpawnpointsCount = table.getn(self.ghostSpawnpoints)
    for key, value in pairs(self.ghosts) do
        position = self.ghostSpawnpoints[1 + key%ghostSpawnpointsCount]
        value.setPosition(value, position.x, position.y)
    end
end

function PacmanGame.draw(self)
    self.bg.draw(self.bg)
    for key, value in pairs(self.ghosts) do
        value.draw(value)
    end
    for key, value in pairs(self.pacmans) do
        value.draw(value)
    end
end

function PacmanGame.update(self)
    inputsUpdate()
    for key, value in pairs(self.ghosts) do
        value.update(value)
    end

    for key, value in pairs(self.pacmans) do
        value.update(value)
    end
end

function PacmanGame.getName()
    return "Pacman"
end

-- GAME SETUP

--ALL_GAMES = {PacmanGame, Game}

KEYBOARD_PPRESETS = {
    playerInputKeyboard("k1", "w", "s", "a", "d", "space", "lctrl"),
    playerInputKeyboard("k2", "up", "down", "left", "right", "rshift", "rctrl")
}

InputSetup = Object.extend(Game)

function InputSetup.load(self)
    UPDATE_COUNTER = 0
    READY_COUNTDOWN = 10
end

function InputSetup.update(self)
    UPDATE_COUNTER = UPDATE_COUNTER +1

    inputsUpdate()

    if UPDATE_COUNTER == 30 then
        UPDATE_COUNTER = 0
        -- print("PRESS A")

        for key, value in pairs(KEYBOARD_PPRESETS) do
            if love.keyboard.isDown(value.keys.a) then
                print("Pressed")
                isThere = false
                for key, input in pairs(inputs) do
                    if value.id == input.id then
                        print("Gamepad already here")
                        isThere = true
                    end
                end
                if not isThere then
                    table.insert(inputs, value)
                    print("Gamepad"..value.id.."added")
                end
            end
        end
        ------------------------------------------------------------------------
        for key, value in pairs(love.joystick.getJoysticks()) do
            if value:isDown(1) then
                print("Pressed")
                id, iid = value:getID()
                isThere = false
                for key, input in pairs(inputs) do
                    if id == input.id then
                        print("Gamepad already here")
                        isThere = true
                    end
                end
                if not isThere then
                    table.insert(inputs, playerInputJoystick(value))
                    print("Gamepad"..value:getName().."added")
                end
            end
            -- print(key)
            -- print(value:getName())
            -- print(value:getID())
        end
        -- TODO: Create checkEveryInputReady function!
        everyone_ready = true
        for key, value in pairs(inputs) do
            value.checkReady(value)
            if not value.ready then everyone_ready = false end
        end
        if everyone_ready then
            READY_COUNTDOWN = READY_COUNTDOWN - 1
        else
            READY_COUNTDOWN = 10
        end
    end
    if READY_COUNTDOWN == 0 then
        activeScene = PacmanSelectScreen()
        activeScene.load(activeScene)
    end
end

function InputSetup.draw(self)
    love.graphics.print("Press A", 20, 20)
    for key, value in pairs(inputs) do
        status = "Not ready"
        if value.ready then status = "Ready" end
        love.graphics.print(value.getName(value).." > "..status, 20, 60 + 20*key)
        
        if READY_COUNTDOWN < 10 then
            love.graphics.print("GAME START IN "..math.floor(READY_COUNTDOWN / 3), 200, 20)
        end
    end
end

-- PACMAN - CHARACTER SELECT

PacmanSelectScreen = Object.extend(Game)

PacmanSelectScreen.COLORS = {
    {  0, 0.5,   1,   1}, -- LIGHT BLUE
    {  1,   0,   0,   1}, -- RED
    {  0,   1,   0,   1}, -- LIME GREEN
    {  0,   1,   1,   1}, -- Turquoise
    {  1,   1,   0,   1}, -- YELLOW
    {  1, 0.5,   0,   1}, -- ORANGE
}

function PacmanSelectScreen.update(self)
    for key, value in pairs(inputs) do
        value.update(value)
        if value.up then
            value.role = 'g'
        end
        if value.down then
            value.role = 'p'
        end
        value.checkReady(value)
    end
    
    everyone_ready = true
    for key, value in pairs(inputs) do
        value.checkReady(value)
        if not value.ready then everyone_ready = false end
    end
    if everyone_ready then
        self.startGame(self)
    end
end

function PacmanSelectScreen.startGame(self)
    ghosts = {}
    pacmans = {}
    clrCount = table.getn(self.COLORS)
    for key, value in pairs(inputs) do
        color = self.COLORS[1 + key%clrCount]
        if value.role == 'g' then
            table.insert(ghosts, {input=value, colorset=color})
        end
        if value.role == 'p' then
            table.insert(pacmans, {input=value, colorset=color})
        end
    end 
    activeScene = PacmanGame(pacmans, ghosts)
    activeScene.load(activeScene)
end

function PacmanSelectScreen.draw(self)
    clrCount = table.getn(self.COLORS)
    drawBegin = 890 - 35 * table.getn(inputs)
    for key, value in pairs(inputs) do
        if value.role == "g" then love.graphics.setColor(self.COLORS[1 + key%clrCount]) else love.graphics.setColor({0.3,0.3,0.3,1}) end
        love.graphics.draw(self.ghost, drawBegin + 70*key, 485)
        if value.role == "p" then love.graphics.setColor(self.COLORS[1 + key%clrCount]) else love.graphics.setColor({0.3,0.3,0.3,1}) end
        love.graphics.draw(self.pacman, drawBegin + 70*key, 555)
    end
end

function PacmanSelectScreen.load(self)
    self.ghost = love.graphics.newImage('sprites/ghost/base.PNG')
    self.pacman = love.graphics.newImage('sprites/pacman/base.PNG')
    for key, value in pairs(inputs) do
        value.ready = false
        value.role = 'g'
    end
end

function PacmanSelectScreen.getName()
    return "Select screen"
end




-- MAIN

function love.load()
    love.window.setMode(1920, 1080)
    activeScene.load(activeScene)
end

function love.draw()
    activeScene.draw(activeScene)
end

function love.update()
    activeScene.update(activeScene)
end


--activeScene = PacmanGame()
activeScene = InputSetup()
--activeScene = PacmanSelectScreen()
